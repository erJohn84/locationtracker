//
//  ViewController.swift
//  LocationTracker
//
//  Created by macbook on 13/11/2016.
//  Copyright © 2016 John Veronelli. All rights reserved.
//

import UIKit
import CoreLocation

class LTViewController: UIViewController, CLLocationManagerDelegate {
    
    @IBOutlet weak var lastUploadTime: UILabel!
    @IBOutlet weak var authStatus: UILabel!
    
    var locationManager: CLLocationManager = CLLocationManager()
    var location = LTLocationClass()
    var minutes: Double = 10 * 60  // time period for server request in minutes
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Initial values for location delegate
        
        authStatus.text = isAuthorized()
        lastUploadTime.text = "0 : 00"
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
        locationManager.allowsBackgroundLocationUpdates = true
        locationManager.requestLocation()
        locationManager.startUpdatingLocation()
        locationManager.requestWhenInUseAuthorization()
    }
    
    override func viewDidAppear(animated: Bool) {
        _ = NSTimer.scheduledTimerWithTimeInterval( minutes, target: self, selector: #selector(LTViewController.update), userInfo: nil, repeats: true) // sends the location to the server in the time period previously declared
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let latestLocation: CLLocation = locations[locations.count - 1]
        location.latitude = String(latestLocation.coordinate.latitude)
        location.longitude = String(latestLocation.coordinate.longitude)
        location.horizontalAccuracy = String(latestLocation.horizontalAccuracy)
        location.altitude = String(latestLocation.altitude)
        location.verticalAccuracy = String(latestLocation.verticalAccuracy)
    }
    
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        print("Location update error: \(error.localizedDescription)")
    }
    
    func update() {
        let request = LTApiHandler()
        request.upload(location) {
            (time: String?) in
            self.lastUploadTime.text = time!
        }
        authStatus.text = isAuthorized()
    }
    
    func isAuthorized() -> String {
        switch(CLLocationManager.authorizationStatus()) {
        case .NotDetermined, .Restricted, .Denied:
            return "Not authorized"
        case .AuthorizedAlways, .AuthorizedWhenInUse:
            return "Authorized"
        }
    }
}


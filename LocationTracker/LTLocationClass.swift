//
//  LTLocationClass.swift
//  LocationTracker
//
//  Created by macbook on 14/11/2016.
//  Copyright © 2016 John Veronelli. All rights reserved.
//

import Foundation

class LTLocationClass { //class that stores location parameters
    var latitude: String!
    var longitude: String!
    var horizontalAccuracy: String!
    var altitude: String!
    var verticalAccuracy: String!
}
//
//  LTApiHandler.swift
//  LocationTracker
//
//  Created by John Veronelli on 15/11/2016.
//  Copyright © 2016 John Veronelli. All rights reserved.
//

import Foundation
import Alamofire

class LTApiHandler { //class that handles the requests to the web service
    
    let baseURLString = "http://requestb.in/1n0kld01"
    
    func upload(location: LTLocationClass, completion: (time: String?) -> Void) {
        
        let requestBody: [String: AnyObject] = [
            "Latitude" : location.latitude,
            "Longitude" : location.longitude,
            "Altitude" : location.altitude,
            "VerticalAccuracy" : location.verticalAccuracy,
            "HorizontalAccuracy" : location.horizontalAccuracy
        ];
        
        Alamofire.request(
            .POST,
            baseURLString,
            parameters: requestBody,
            encoding: .JSON).validate().responseString { (response) in
            switch response.result {
            case .Success( _):
                let date = NSDate()
                let calender = NSCalendar.currentCalendar()
                let components = calender.components([.Hour, .Minute], fromDate: date)
                let hour = String(components.hour) + " : " + String(components.minute)
                completion(time: hour)
            case .Failure(let error):
                completion(time: String(error))
            }
        }
    }
}